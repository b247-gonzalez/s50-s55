const coursesData = [
    {
        id : "wdc001",
        name : "PHP - Laravel",
        description : "Laravel is a free and open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller architectural pattern and based on Symfony.",
        price : 45000,
        enrollees : 0,
        onOffer : true
    },
    {
        id : "wdc002",
        name : "Python - Django",
        description : "Python Django is a framework for web applications, as it allows developers to use modules for faster development. As a web developer, you can use these modules to create apps and websites from an existing source. It speeds up the development process significantly, as you do not have to code everything from scratch.",
        price : 50000,
        enrollees : 0,
        onOffer : true
    },
    {
        id : "wdc003",
        name : "Java - Spring Boot",
        description : "Java Spring Boot (Spring Boot) is a tool that makes developing web application and microservices with Spring Framework faster and easier through three core capabilities: Autoconfiguration. An opinionated approach to configuration. The ability to create standalone applications.",
        price : 55000,
        enrollees : 0,
        onOffer : true
    },
]

export default coursesData;