import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import CourseView from './components/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Error from './pages/Error';
import Logout from './pages/Logout';
import './App.css';

import { Container } from 'react-bootstrap';
import { Fragment, useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {
    // const [user, setUser] = useState({ email : localStorage.getItem("email") });
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });
    
    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            }
            else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })
    });

    return (
        <Fragment>
            <UserProvider value={{ user, setUser, unsetUser }}>
                <Router>
                    <AppNavbar />
                    <Container>
                        <Routes>
                            <Route path="/" element={<Home />} />
                            <Route path="/courses" element={<Courses />} />
                            <Route path="/courses/:courseId" element={<CourseView />} />
                            <Route path="/logout" element={<Logout />} />
                            <Route path="/login" element={<Login />} />
                            <Route path="/signup" element={<Register />} />
                            <Route path="*" element={<Error />} />
                        </Routes>
                    </Container>    
                </Router>
            </UserProvider>
        </Fragment>
    );
}

export default App;
