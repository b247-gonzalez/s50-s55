import { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from '../UserContext';

export default function Login(props) {
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const passReg = RegExp(/^[a-zA-Z0-9!@#$%^&*]{6,}$/);
    const emailReg = RegExp(/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);

    function loginUser(e) {
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.access !== "undefined") {
                localStorage.setItem("token", data.access);

                retreiveUserDetails(localStorage.getItem("token"));
                Swal.fire({
                    title: 'Login successful!',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                });
            }
            else {
                Swal.fire({
                    title: 'Authentication failed!',
                    icon: 'error',
                    text: 'Please check your login details and try again.'
                });
            }
        });
        
        setEmail("");
        setPassword("");
    }

    const emailExist = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                loginUser();
            }
            else {
                Swal.fire({
                    title: "User not exist!",
                    icon: 'error',
                    text: 'Check your credentials or register new user.'
                });
            }
        })
    }

    const retreiveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorizations: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });
        });
    };

    useEffect(() => {
        if (((email !== "") && (emailReg.test(email))) && ((password !== "") && (password.length >= 8)) && (passReg.test(password))) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [email, password, emailReg, passReg]);

    return (
        (user.id !== null) ?
            <Navigate to="/courses" />
        :
        <Form onSubmit={emailExist} className="mt-4">
            <h1>Login</h1>
            <Form.Group controlId="email">
                <Form.Label>Email:</Form.Label>
                <Form.Control type="email" placeholder="Enter email here" value={email} onChange={e => setEmail(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password:</Form.Label>
                <Form.Control type="password" placeholder="Enter password here" value={password} onChange={e => setPassword(e.target.value)} required />
            </Form.Group>

            { isActive ?
            <Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
            :
            <Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
            }
        </Form>
    );
}