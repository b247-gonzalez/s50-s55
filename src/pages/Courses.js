// import coursesData from "../data/coursesData";
import CourseCard from "../components/CourseCard";
import { Container } from "react-bootstrap";
import { useEffect, useState } from "react";

export default function Courses() {
    /*
    const courses = coursesData.map(course => {
        return (
            <CourseCard key={course.id} course={course} />
        )
    })
    */

    const [ courses, setCourses] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
        .then(res => res.json())
        .then(data => {
            setCourses(data.map(course => {
                return (
                    <CourseCard key={course._id} course={course} />
                );
            }))
        });
    }, []);

    return (
        <>
            <Container className="mt-4">
                {courses}
            </Container>
        </>
    )
};