import { useContext, useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
    // eslint-disable-next-line no-unused-vars
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [isActive, setIsActive] = useState(false);
    const nameReg = RegExp(/^[^-\s][a-zA-Z_\s-]+$/);
    const passReg = RegExp(/^[a-zA-Z0-9!@#$%^&*]{6,}$/);
    const mobileReg = RegExp(/^[0-9]{6,}$/);
    const emailReg = RegExp(/^[A-Z0-9._-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);
    
    function registerUser(e) {
        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNumber: mobileNumber,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: 'Registration successful!',
                    icon: 'success',
                    text: 'You may now login with your email and password.'
                });
                navigate("/login");
            }
            else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }
        })

        setFirstName("");
        setLastName("");
        setEmail("");
        setMobileNumber("");
        setPassword1("");
        setPassword2("");
    }

    const emailExist = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Duplicate email found!",
                    icon: 'error',
                    text: 'Please provide a different email.'
                });
            }
            else {
                registerUser();
            }
        })
    }

    useEffect(() => {
        if (((firstName !== "") && (nameReg.test(firstName))) && ((lastName !== "") && (nameReg.test(lastName))) && (email !== "" && password1 !== "" && password2 !== "") && ((mobileNumber !== "") && (mobileReg.test(mobileNumber)) && (mobileNumber.length >= 11)) && (password1 === password2) && (password1.length && password2.length >= 8) && (passReg.test(password1) && passReg.test(password2) && emailReg.test(email))) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    },[firstName, lastName, email, mobileNumber, password1, password2, nameReg, passReg, emailReg, mobileReg]);

    return (
        (user.id !== null) ?
            <Navigate to="/courses" />
        :
        // <Form onSubmit={(e) => registerUser(e)} className="mt-4">
        <Form onSubmit={emailExist} className="mt-4">
        <h1>Registration</h1>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" placeholder="Enter first name here" value={firstName} onChange={e => setFirstName(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Enter last name here" value={lastName} onChange={e => setLastName(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="email">
                <Form.Label>Email Address</Form.Label>
                <Form.Control type="email" placeholder="Enter email here" value={email} onChange={e => setEmail(e.target.value)} required/>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number:</Form.Label>
                <Form.Control type="text" placeholder="Enter mobile number here" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required />
            </Form.Group>

            { isActive ?
                <Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
                :
                <Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
            }
        </Form>
    );
}