import { Button, Card } from "react-bootstrap";
import PropTypes from "prop-types"
import { Link } from "react-router-dom";

export default function CourseCard({course}) {
    const {_id, name, description, price} = course;
    
    return (
        <Card className="courseCard">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>
                    {description}
                </Card.Text>

                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                
                {/* <Card.Text className="mb-0">Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text> */}
                <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    );
};

CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

// My Code
// import { Button, Card, Col, Row } from "react-bootstrap";

/*
export default function CourseCard() {
    return (
        <Row className="mt-3 mb-3">
            <Col>
                <Card className="courseCard p-3">
                    <Card.Body>
                        <Card.Title>
                            <h3>Sample Course</h3>
                        </Card.Title>
                        <Card.Text>
                            <h5>Description</h5>
                            <p className="mb-3">This is a sample course offering.</p>

                            <h5>Description</h5>
                            <p>PhP. 40,000</p>
                        </Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
};
*/