import { Nav, Navbar } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
// eslint-disable-next-line no-unused-vars
import { useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar() {
    // eslint-disable-next-line no-unused-vars
    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg" className='px-4'>
            <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                { (user.id !== null) ?
                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                    :
                    <>
                        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                        <Nav.Link as={NavLink} to="/signup">Register</Nav.Link>
                    </>
                }                
            </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};